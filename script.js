console.log("hello world")

let courses = [
  {
    id: "1",
    name: "Python 101",
    description: "Learn the basics of python.",
    price: 15000,
    isActive: true,
  },
  {
    id: "2",
    name: "CSS 101",
    description: "Learn the basics of CSS.",
    price: 10500,
    isActive: true,
  },
  {
    id: "3",
    name: "CSS 102",
    description: "Learn an advanced CSS.",
    price: 15000,
    isActive: false,
  },
  {
    id: "4",
    name: "PHP-Laravel 101",
    description: "Learn the basics of PHP and its Laravel framework.",
    price: 20000,
    isActive: true,
  },
];

let addCourse = (id, name, description, price, isActive) => {
  let object = {
    id: `${ id }`,
    name: `${ name }`,
    description: `${ description }`,
    price: `${ price }`,
    isActive: `${ isActive }`,
  };
  courses.push( object );
  alert(`You have created ${name}. Its price is ${price}.`);
};
addCourse("5", "HTML 101", "Learn basics of HTML", 10000, true);
console.log(courses);

let getSingleCourse = (id) => {
  let find = courses.find((courses) => courses.id === id);
  return find;
};
console.log(getSingleCourse("5"));

let getAllCourses = () => {
  courses.forEach((course) => {
    console.log(course);
    return course;
  
  });

};

getAllCourses();

let archiveCourse = (id) => {
  courses[id].isActive = false;
  console.log(courses[id]);
};

archiveCourse(1);

let deleteCourse = () => {
  courses.pop();
  console.log(courses);
};

deleteCourse();

let showActiveCourses = () => {
  let active = courses.filter((course) => {
    return course.isActive == true;
  });
  console.log(active);
};

showActiveCourses();
